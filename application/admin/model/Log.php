<?php
/**
 * Created by 流年酷.
 * User: WenZhen Miao
 * Date: 2018-04-11
 * Time: 下午 4:15
 */

namespace app\admin\model;
use think\Model;
//记录日志
class Log extends Model
{
	// 当前模型对应的数据表名称
	protected $table = 'lnk_log';
	protected $createTime = 'addtime';
	protected $autoWriteTimestamp = true;
	protected $pk = 'id';

	/**
	 * [login 添加日志]
	 * @Author WenZhen Miao
	 * @DateTime  2018-04-12T10:45:33+0800
	 * @param     [string]                   $action [操作url]
	 * @param     [string]                   $desc      [操作描述]
	 * @return    [type]                               [description]
	 */
	public function add_log($action,$desc){
			$log = Log::create([
				'admin_id'=>is_login(),
				'action'=>$action,
				'addip'=>ip2long(request()->ip()),
				'desc'=>$desc
			]);
            return $log;
	}
	/**
	 * [login 删除日志]
	 * @Author WenZhen Miao
	 * @DateTime  2018-04-12T10:45:33+0800
	 * @param     [string]                   $id [日志id]
	 * @param     [string]                   $type [日志类型]
	 * @return    [type]                               [description]
	 */
	public function del_log($id,$type=0){
		switch ($type){
			case 1:
				$show=Db('Log')->delete(true);
				break;
			default:
				$show=Log::destroy($id);
				break;
		}
		if(!$show){
			$this->error = '删除失败';
			return false;
		}
		return $show;

	}
}