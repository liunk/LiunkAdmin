<?php
/**
 * Created by 流年酷.
 * User: WenZhen Miao
 * Date: 2018-04-11
 * Time: 上午 10:10
 */

namespace app\admin\controller;
use app\admin\model\Log;
use app\admin\model\User;
use think\Controller;
use think\captcha\Captcha;
use think\facade\Request;
use think\facade\Url;

/*登录相关资源目录*/
/**
 * @route('public')
 */
class Publics extends Controller
{
	protected $request;

	/**
	 * @param  string  $name 后台登录页
	 * @return mixed
	 * @route('public/index','get')
	 */
      public function index(){
      //	dump(session('user_auth'));

	      return view('', ['title' => '后台登陆','verify'=>Url::build('public/verify','random='.time())]);
      }
	/**
	 * @param  string  $name 登录操作
	 * @return mixed
	 * @route('public/login')
	 */
	public function login(){
		if($this->request->isPut()){
			$user = model('User');
			$data=Request::only(['username','password','verify'], 'put');
			$show=$user->login($data['username'],$data['password'],$data['verify']);
			if(!$show){
				return json(['status'=>0,'info'=>$user->getError()]);
			}else{
				return json(['status'=>1,'info'=>'登录成功','url'=>Url::build('index/index')]);
			}
		}else{
			exit(0);
		}

	}
	/**
	 * @param  string 二维码
	 * @return mixed
	 * @route('public/verify')
	 */
	public function verify(){
		$config =    [
			// 验证码字体大小
			'fontSize'    =>    20,
			// 验证码位数
			'length'      =>    4,
			// 关闭验证码杂点
			'useNoise'    =>    false,
		];
		$captcha = new Captcha($config);
		return $captcha->entry();
	}
}