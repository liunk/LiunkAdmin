<?php
/**
 * Created by 流年酷.
 * User: WenZhen Miao
 * Date: 2018-04-09
 * Time: 下午 6:01
 */

namespace app\admin\controller;
use think\Controller;
use think\Auth;

class Base extends Controller
{
	protected $lesson;
	protected $log;
	public function initialize()
	{

		// 获取当前用户ID
		if(defined('UID')) return ;
		define('UID',is_login());
		if( !UID ){// 还没登录 跳转到登录页面
			$this->redirect('public/index');exit;
		}
		//加载日志模型
		$this->log=model('Log');

		//全称判断登录是否失效
		if(!is_login_token()){
			$this->redirect('public/index');exit;
		}

	}
}